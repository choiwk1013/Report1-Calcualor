package kr.ac.kpu.cs.report1;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
    private TextView mTextView1;
    private TextView mTextView2;

    private String operand1 = "0";
    private String operand2 = "";
    private char op = ' ';
    private boolean inEqual = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView1 = (TextView) findViewById(R.id.textView);
        mTextView2 = (TextView) findViewById(R.id.textView2);

        refresh();
    }

    public void onClick(View v){
        Button b = (Button)v;

        switch(v.getId()){
            case R.id.num0:
            case R.id.num1:
            case R.id.num2:
            case R.id.num3:
            case R.id.num4:
            case R.id.num5:
            case R.id.num6:
            case R.id.num7:
            case R.id.num8:
            case R.id.num9:
                String num = b.getText().toString();

                //  연잔사가 입력되지 않으면 첫번째 피연산자에 입력
               if(op == ' ') {

                   //   = 이 입력된 다음 숫자 버튼을 누를 경우 처음부터 다시 입력한다.
                   //   3 + 1 = 입력
                   //   <계산기>
                   //   --> ""
                   //   --> "4"
                   //
                   //   3입력
                   //   <계산기>
                   //   --> ""
                   //   --> "3"
                   //
                    if(inEqual) {
                        operand1 = "";
                        inEqual = false;
                    }
                    operand1 += num;
                    operand1 = "" + str2int(operand1);  //  00001 같은 숫자를 --> 1 로 변경하기위해
                }

                //  연산자가 입력된 경우이므로두번째 피연산자에 입력
                else {
                    operand2 += num;
                    operand2 = "" + str2int(operand2);
                }
                break;

            case R.id.plus:
            case R.id.minus:
            case R.id.multi:
            case R.id.div:
                //  연산자1, 연산자2가 모두 입력된 상태에서 다시 연산자가 입력될 경우 연속해서 계산( '=' 버튼을 눌렀을때처럼 계산을 수행 )
                //  막약 '=' 버튼으로 연산을 마쳤다면 operand2의 값을 제거하여 조건이 성립되지 않음
                if(operand1.length() != 0 && operand2.length() != 0) {
                    calc(true);
                }

                op = b.getText().toString().charAt(0);
                inEqual = false;
                break;

            case R.id.result:
                //  피연산자1, 피연산자2, 연산자 셋 중 어느하나라도 입력되지 않으면 = 버튼을 눌렀을 때 무시
                if(op == ' ' || operand1.length() == 0 || operand2.length() == 0) break;

                calc(false);
                inEqual = true; //  '=' 버튼을 누른 다음에 숫자가 입력될 때를 구분하기 위해서
                break;

            case R.id.clear:
                op = ' ';
                operand1 = "";
                operand2 = "";
                break;
        }

        Toast.makeText(this, String.format("OR1 : %s, OR2 : %s, OP : %c", operand1, operand2, op), Toast.LENGTH_LONG).show();
        refresh();
    }

    /**
     * operand1, operand2, op 이 세가지 버튼을 기반으로<br/>
     * 계산기의 두 화변에 표시되 것을 변경하여줌
     */
    private void refresh() {
        //  아무 것도 입력되지 않은 상태 또는 C 버튼을 누른 상태
        if(operand1.equals("0") && op == ' ') {
            mTextView1.setText("");
            mTextView2.setText(operand1);
        }

        //  첫번재 피 연산자를 입력하는 상태, 연산자는 입력하지 않음
        //  또는 3 + 1 = 과 같이 계산 결과를 출력할 때
        //  operand1에 계산한 결과가 담긴다.
       else if(op == ' ') {
            mTextView1.setText("");
            mTextView2.setText(operand1);
        }

        //  첫번째 피연산자 입력을 마치고 연산자를 입력한 상태
        else if(op != ' ' && operand2.length() == 0) {
            mTextView1.setText(operand1 + op);
            mTextView2.setText(operand1);
        }

        //  두번째 피연산자까지 입력을 였을 때
        else if(op != ' ' && operand2.length() != 0) {
            mTextView2.setText(operand2);
        }
    }

    /**
     * 수식을 입력후 '=' 버튼이나 다른 연산자 버튼을 눌렀을 때 연산을 수행하는 메소<br/>
     * ex) 2 + 1 = 을 눌렀을 때 계산, 2 + 1 + 를 눌렀을 때 계
     *
     * @param con '=' 버튼이 아니라 다른 연산자를 눌러서 계산할 때는 true
     */
    private void calc(boolean con) {
        int n1 = Integer.parseInt(operand1);
        int n2 = Integer.parseInt(operand2);
        int result = 0;

        switch(op) {
            case '+':
                result = n1 + n2;
                break;
            case '-':
                result = n1 - n2;
                break;
            case '*':
                result = n1 * n2;
                break;
            case '/':
                result = n1 / n2;
                break;
        }

        //  계산을 마치면 operand1에 계산 결과를 넣어준다.
        //  operand2의 값은 제거
        //  op의 경우 '='을 눌러 마쳤다면 연산자를 제거, '+-*/'와 같은 연산자로 계산을 하였다면 연산자 유지
        operand1 = "" + result;
        operand2 = "";
        op = !con ? ' ' : op;
    }

    /**
     * 문자열로 된 수를 int 타입으로 변경하여 줌
     *
     * @param str 문자열 타입의 수 ex) "1234"
    * @return int 타입의 숫자
     */
    private int str2int(String str) {
        return Integer.parseInt(str);
    }
}
